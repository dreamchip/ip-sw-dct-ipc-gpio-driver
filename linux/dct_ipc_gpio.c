/*
 * Copyright (C) 2024 Dream Chip Technologies
 *
 * SPDX-License-Identifier: GPL-2.0-only
 */

#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/of_platform.h>
#include <linux/mod_devicetable.h>
#include <linux/of.h>
#include <linux/io.h>

#include <linux/gpio/driver.h>
#include <linux/of_address.h>
#include <linux/interrupt.h>

#include <dct_ipc_gpio.h>
#include <dct_ipc.h>
#include <dct_ipc_fsp.h>

#define MAX_NR_OF_PINS 32
#define GPIO_OUTPUT_ALCATRAZ (1U << 17)
#define GPIO_INPUT_ALCATRAZ (1U << 16)

#define DST IPC_FSP
#define CHANNEL 0
#define IPC_RETRY_TIMEOUT_MS 20u

struct dct_ipc_gpio_driver_data {
	struct gpio_chip gc;
	struct device *dev;
	struct platform_device *pdev;
	struct platform_device *ipc_dev;
	uint32_t ngpios;
	spinlock_t lock;
};

static struct platform_driver dct_ipc_gpio_driver;

static int dct_dct_gpio_get(struct gpio_chip *gc, unsigned off)
{
	const struct dct_ipc_gpio_driver_data *driver_data =
		gpiochip_get_data(gc);
	int rc = (driver_data->ngpios > off) ? (0) : (-EINVAL);
	if (0 == rc) {
		struct ipc_fsp_spp_gpio_get_raw tx_gpio_get_raw;
		struct ipc_fsp_spp_gpio_get_raw rx_gpio_get_raw;
		uint32_t max_rx_data_len;
		memset(&tx_gpio_get_raw, 0x00, sizeof(tx_gpio_get_raw));
		memset(&rx_gpio_get_raw, 0x00, sizeof(rx_gpio_get_raw));
		tx_gpio_get_raw.hdr.cmd = IPC_FSP_SPP_GPIO_GET_RAW;
		tx_gpio_get_raw.hdr.len =
			sizeof(struct ipc_fsp_spp_gpio_get_raw) -
			sizeof(struct ipc_header);
		max_rx_data_len = sizeof(rx_gpio_get_raw.rx_value);
		rc = dct_ipc_send_receive_timeout(driver_data->ipc_dev,
				IPC_FSP, CHANNEL,
				(struct ipc_header *)&tx_gpio_get_raw,
				(struct ipc_header *)&rx_gpio_get_raw,
				max_rx_data_len, IPC_RETRY_TIMEOUT_MS);
		if (0 == rc) {
			rc = (0 != (rx_gpio_get_raw.rx_value & (1U << off))) ?
				     (1) :
				     (0);
		} else {
			dev_err(gc->parent,
				"dct_ipc_send_receive failed with rc = %d\n",
				rc);
		}
	} else {
		dev_err(gc->parent, "off = %u exceeds maximum of %d\n", off,
			driver_data->ngpios);
	}

	return rc;
}

static void dct_ipc_gpio_set(struct gpio_chip *gc, unsigned off, int val)
{
	const struct dct_ipc_gpio_driver_data *driver_data =
		gpiochip_get_data(gc);
	const uint32_t pins = 1U << off;
	int rc = (driver_data->ngpios > off) ? (0) : (-EINVAL);

	if (0 == rc) {
		if (val != 0) {
			struct ipc_fsp_spp_gpio_set_bits_raw gpio_set_bits_raw;
			memset(&gpio_set_bits_raw, 0x00,
			       sizeof(gpio_set_bits_raw));
			gpio_set_bits_raw.hdr.cmd =
				IPC_FSP_SPP_GPIO_SET_BITS_RAW;
			gpio_set_bits_raw.hdr.len =
				sizeof(struct ipc_fsp_spp_gpio_set_bits_raw) -
				sizeof(struct ipc_header);
			gpio_set_bits_raw.pins = pins;
			rc = dct_ipc_send_timeout(driver_data->ipc_dev,
				IPC_FSP, CHANNEL,
				(struct ipc_header *)&gpio_set_bits_raw,
				IPC_RETRY_TIMEOUT_MS);
		} else {
			struct ipc_fsp_spp_gpio_clr_bits_raw gpio_clr_bits_raw;
			memset(&gpio_clr_bits_raw, 0x00,
			       sizeof(gpio_clr_bits_raw));
			gpio_clr_bits_raw.hdr.cmd =
				IPC_FSP_SPP_GPIO_CLR_BITS_RAW;
			gpio_clr_bits_raw.hdr.len =
				sizeof(struct ipc_fsp_spp_gpio_clr_bits_raw) -
				sizeof(struct ipc_header);
			gpio_clr_bits_raw.pins = pins;
			rc = dct_ipc_send_timeout(driver_data->ipc_dev,
				IPC_FSP, CHANNEL,
				(struct ipc_header *)&gpio_clr_bits_raw,
				IPC_RETRY_TIMEOUT_MS);
		}
		if (0 != rc) {
			dev_err(gc->parent,
				"dct_ipc_send failed with rc = %d\n", rc);
		}
	} else {
		dev_err(gc->parent, "off = %u exceeds maximum of %d\n", off,
			driver_data->ngpios);
	}

	return;
}

static int dct_ipc_gpio_direction_input(struct gpio_chip *gc, unsigned off)
{
	const struct dct_ipc_gpio_driver_data *driver_data =
		gpiochip_get_data(gc);
	int rc = (driver_data->ngpios > off) ? (0) : (-EINVAL);

	if (0 == rc) {
		struct ipc_fsp_spp_gpio_configure gpio_configure;
		memset(&gpio_configure, 0x00, sizeof(gpio_configure));
		gpio_configure.hdr.cmd = IPC_FSP_SPP_GPIO_CONFIGURE;
		gpio_configure.hdr.len =
			sizeof(struct ipc_fsp_spp_gpio_configure) -
			sizeof(struct ipc_header);
		gpio_configure.pin = off;
		gpio_configure.flags = GPIO_INPUT_ALCATRAZ;
		rc = dct_ipc_send_timeout(driver_data->ipc_dev,
				IPC_FSP, CHANNEL,
				(struct ipc_header *)&gpio_configure,
				IPC_RETRY_TIMEOUT_MS);
		if (0 != rc) {
			dev_err(gc->parent,
				"dct_ipc_send failed with rc = %d\n", rc);
		}
	} else {
		dev_err(gc->parent, "off = %u exceeds maximum of %d\n", off,
			driver_data->ngpios);
	}

	return rc;
}

static int dct_ipc_gpio_direction_output(struct gpio_chip *gc, unsigned off,
					 int val)
{
	const struct dct_ipc_gpio_driver_data *driver_data =
		gpiochip_get_data(gc);
	int rc = (driver_data->ngpios > off) ? (0) : (-EINVAL);

	if (0 == rc) {
		struct ipc_fsp_spp_gpio_configure gpio_configure;
		memset(&gpio_configure, 0x00, sizeof(gpio_configure));
		gpio_configure.hdr.cmd = IPC_FSP_SPP_GPIO_CONFIGURE;
		gpio_configure.hdr.len =
			sizeof(struct ipc_fsp_spp_gpio_configure) -
			sizeof(struct ipc_header);
		gpio_configure.pin = off;
		gpio_configure.flags = GPIO_OUTPUT_ALCATRAZ;
		rc = dct_ipc_send_timeout(driver_data->ipc_dev,
				IPC_FSP, CHANNEL,
				(struct ipc_header *)&gpio_configure,
				IPC_RETRY_TIMEOUT_MS);
		if (0 != rc) {
			dev_err(gc->parent,
				"dct_ipc_send failed with rc = %d\n", rc);
		}
	} else {
		dev_err(gc->parent, "off = %u exceeds maximum of %d\n", off,
			driver_data->ngpios);
	}

	return rc;
}

static int dct_ipc_gpio_get_direction(struct gpio_chip *gc, unsigned off)
{
	const struct dct_ipc_gpio_driver_data *driver_data =
		gpiochip_get_data(gc);
#if 0
	struct dct_ipc_gpio_driver_data *p = gpiochip_get_data(gc);
	u32 val;

	val = ioread32(p->base + DCT_IPC_GPIO_OE);

	dev_info(p->dev, "%s 0x%x=0x%x\n", __func__, DCT_IPC_GPIO_OE, val);

	return !!(val & BIT(off));
#endif
	return 0;
}

static int dct_ipc_gpio_probe(struct platform_device *pdev)
{
	int ret = 0;
	struct gpio_chip *gc;
	struct device *dev = &pdev->dev;
	struct device_node *np = dev->of_node;
	struct dct_ipc_gpio_driver_data *driver_data;
	const char *dct_ipc_name = "dct-ipc";
	struct platform_device *ipc_dev;
	const char *name = dev_name(dev);
	uint32_t ngpios = 32;

	dev_info(dev, "probing ipc_gpio ...\n");

	ipc_dev = dct_devm_ipc_get_byphandle(dev, np, dct_ipc_name);
	if (IS_ERR(ipc_dev)) {
		if (PTR_ERR(ipc_dev) == -EAGAIN) {
			dev_info(dev, "waiting for dct-ipc device\n");
			return -EPROBE_DEFER;
		} else {
			dev_err(dev, "failed to retrieve dct-ipc device\n");
			return -EINVAL;
		}
	}
	if (WARN_ON(!ipc_dev)) {
		return -EINVAL;
	}

	if (0 != of_property_read_u32(np, "ngpios", &ngpios)) {
		ngpios = MAX_NR_OF_PINS;
	} else if (ngpios > MAX_NR_OF_PINS) {
		dev_err(dev, "ngpios = %u exceeds maximum of %d\n", ngpios,
			MAX_NR_OF_PINS);
		return -EINVAL;
	}

	driver_data = devm_kzalloc(dev, sizeof(*driver_data), GFP_KERNEL);
	if (!driver_data) {
		dev_err(dev, "devm_kzalloc for driver_data failed\n");
		return -ENOMEM;
	}
	memset(driver_data, 0x00, sizeof(*driver_data));
	driver_data->dev = dev;
	driver_data->pdev = pdev;
	driver_data->ipc_dev = ipc_dev;
	driver_data->ngpios = ngpios;
	spin_lock_init(&driver_data->lock);

	driver_data->gc.label = name;
	driver_data->gc.owner = THIS_MODULE;
	driver_data->gc.get = dct_dct_gpio_get;
	driver_data->gc.set = dct_ipc_gpio_set;
	driver_data->gc.direction_output = dct_ipc_gpio_direction_output;
	driver_data->gc.direction_input = dct_ipc_gpio_direction_input;
	driver_data->gc.get_direction = dct_ipc_gpio_get_direction;
	driver_data->gc.can_sleep = true;
	driver_data->gc.base = -1;
	driver_data->gc.ngpio = ngpios;
	driver_data->gc.parent = dev;

	ret = gpiochip_add_data(&(driver_data->gc), driver_data);
	if (ret) {
		dev_err(dev, "failed to register GPIO %s\n", name);
		return ret;
	}

	platform_set_drvdata(pdev, driver_data);
	dev_info(dev, "DCT IPC GPIO initialized\n");
	return 0;
}

static int dct_ipc_gpio_remove(struct platform_device *pdev)
{
	return 0;
}

static const struct of_device_id dct_ipc_gpio_match[] = {
	{ .compatible = "dct,dct-ipc-gpio", .data = 0 },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, dct_ipc_gpio_match);

static struct platform_driver dct_ipc_gpio_driver = {
	.driver = {
		.name = "dct_ipc_gpio",
		.probe_type = PROBE_PREFER_ASYNCHRONOUS,
		.of_match_table = dct_ipc_gpio_match,
	},
	.probe = dct_ipc_gpio_probe,
	.remove = dct_ipc_gpio_remove,
};

static int __init dct_ipc_gpio_init_module(void)
{
	int ret;

	ret = platform_driver_register(&dct_ipc_gpio_driver);
	if (ret)
		return ret;

	return ret;
}

static void __exit dct_ipc_gpio_exit_module(void)
{
	platform_driver_unregister(&dct_ipc_gpio_driver);
}

module_init(dct_ipc_gpio_init_module);
module_exit(dct_ipc_gpio_exit_module);

MODULE_DESCRIPTION("zukimo ipc gpio driver");
MODULE_LICENSE("GPL");
